package com.kenfogel.data;

import java.math.BigDecimal;

public class FinanceBean {

    private BigDecimal payment;
    private BigDecimal presentValue;
    private BigDecimal rate;
    private BigDecimal term;
    private BigDecimal futureValue;

    public FinanceBean() {
        payment = new BigDecimal("0");
        presentValue = new BigDecimal("0");
        rate = new BigDecimal("0");
        term = new BigDecimal("0");
        futureValue = new BigDecimal("0");
    }

    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }

    public BigDecimal getPresentValue() {
        return presentValue;
    }

    public void setPresentValue(BigDecimal presentValue) {
        this.presentValue = presentValue;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getTerm() {
        return term;
    }

    public void setTerm(BigDecimal term) {
        this.term = term;
    }

    public BigDecimal getFutureValue() {
        return futureValue;
    }

    public void setFutureValue(BigDecimal futureValue) {
        this.futureValue = futureValue;
    }

    @Override
    public String toString() {
        return "FinanceBean [payment=" + payment + ", presentValue="
                + presentValue + ", rate=" + rate + ", term=" + term
                + ", futureValue=" + futureValue + "]";
    }
}
