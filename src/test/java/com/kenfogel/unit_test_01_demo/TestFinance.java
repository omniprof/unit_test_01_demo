package com.kenfogel.unit_test_01_demo;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import com.kenfogel.business.Calculations;
import com.kenfogel.data.FinanceBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to test is formulas used in Calculations.java are accurate 
 * 
 * Loan PV 5000 rate 0.004166667 term 60 PMT $94.36 
 * 
 * Future PMT 100 rate 0.004166667 term 60 FV $6,800.61 
 * 
 * Savings FV 6800.61 rate 0.004166667 term 60 PMT $100.00
 *
 * @author Ken Fogel
 */
public class TestFinance {

    private final static Logger LOG = LoggerFactory.getLogger(TestFinance.class);

    private Calculations calc;
    private FinanceBean fb;

    /**
     * Prepare the objects that will be re-used
     */
    @Before
    public void init() {
        calc = new Calculations();
        fb = new FinanceBean();
    }

    /**
     * Valid loan test data
     */
    @Test
    public void knowValueLoanCalculationTest() {
        fb.setPresentValue(new BigDecimal("5000"));
        fb.setRate(new BigDecimal("0.004166667"));
        fb.setTerm(new BigDecimal("60"));
        calc.loanCalculation(fb);
        LOG.info(fb.toString());
        assertEquals(new BigDecimal("94.37"), fb.getPayment());
    }

    /**
     * Valid future data
     */
    @Test
    public void knowValueFutureCalculationTest() {
        fb.setPayment(new BigDecimal("100"));
        fb.setRate(new BigDecimal("0.004166667"));
        fb.setTerm(new BigDecimal("60"));
        calc.futureValueCalculation(fb);
        LOG.info(fb.toString());
        assertEquals(new BigDecimal("6800.61"), fb.getFutureValue());
    }

    /**
     * Valid savings data
     */
    @Test
    public void knowValueSavingsCalculationTest() {
        fb.setFutureValue(new BigDecimal("6800.61"));
        fb.setRate(new BigDecimal("0.004166667"));
        fb.setTerm(new BigDecimal("60"));
        calc.savingsGoalCalculation(fb);
        LOG.info(fb.toString());
        assertEquals(new BigDecimal("100.00"), fb.getPayment());
    }

    /**
     * Invalid loan data
     */
    @Test(expected = ArithmeticException.class)
    public void badDataTest() {
        fb.setPresentValue(new BigDecimal("0"));
        fb.setRate(new BigDecimal("0"));
        fb.setTerm(new BigDecimal("0"));
        LOG.info("BadData: " + fb.toString());
        calc.loanCalculation(fb);
        fail("Did not throw ArithmeticException");
    }
}
